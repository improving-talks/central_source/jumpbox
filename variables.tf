variable "environment" {
  description = "Name of the environment (Ex. `dev`, `uat`, `prod`)"
  default     = ""
}

variable "vpc_id" {
  description = "VPC ID for Jumpbox"
}

variable "security_group_ids" {
  type        = list(string)
  description = "List of Security Group IDs for Jumpbox"
  default     = []
}

variable "subnet_id" {
  description = "Subnet ID to deploy the Jumpbox to"
}

variable "instance_type" {
  description = "What instance type to launch for Jumpbox"
  default     = "t2.micro"
}

variable "os_platform" {
  description = "OS to launch with Jumpbox"
  default     = "linux"
}

variable "tags" {
  description = "Tags to apply to the resources created by this module"
  type        = map(string)
  default     = {}
}

locals {
  tags = merge(var.tags,
    {
      service = "network"
  })
}