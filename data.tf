data "aws_region" "current" {}

data "aws_caller_identity" "current" {}

data "aws_ami" "default" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = [local.ami_name]
  }
}

locals {
  windows_ami_name = var.os_platform == "windows" ? "Windows_Server-2019-English-Full-Base-*" : ""
  linux_ami_name   = var.os_platform == "linux" ? "amzn2-ami-hvm-2.0.*-x86_64-gp2" : ""
  ami_name         = coalesce(local.windows_ami_name, local.linux_ami_name, "")
}