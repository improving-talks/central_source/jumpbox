output "instance_id" {
  value = element(concat(aws_instance.instance.*.id, [""]), 0)
}

output "instance_profile_arns" {
  value = aws_iam_role.default.*.arn
}

output "security_group_id" {
  value = join("\",\"", aws_security_group.jumpbox[*].id)
}
