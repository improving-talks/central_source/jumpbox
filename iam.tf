resource "aws_iam_instance_profile" "default" {
  name = "jumpbox-${var.environment}"
  role = aws_iam_role.default.*.name[0]
}

resource "aws_iam_role" "default" {
  name = "jumpbox-${var.environment}"
  path = "/"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "",
            "Effect": "Allow",
            "Principal": {
                "Service": [
                "ec2.amazonaws.com",
                "ssm.amazonaws.com"
            ]
            },
            "Action": "sts:AssumeRole"
        }
    ]
}
EOF


  tags = local.tags
}

data "aws_iam_policy_document" "ec2_role_for_ssm" {
  statement {
    effect    = "Allow"
    resources = ["*"]

    actions = [
      "ssm:DescribeAssociation",
      "ssm:GetDeployablePatchSnapshotForInstance",
      "ssm:GetDocument",
      "ssm:DescribeDocument",
      "ssm:GetManifest",
      "ssm:GetParameters",
      "ssm:ListAssociations",
      "ssm:ListInstanceAssociations",
      "ssm:PutInventory",
      "ssm:PutComplianceItems",
      "ssm:PutConfigurePackageResult",
      "ssm:UpdateAssociationStatus",
      "ssm:UpdateInstanceAssociationStatus",
      "ssm:UpdateInstanceInformation",
    ]
  }

  statement {
    effect    = "Allow"
    resources = ["*"]

    actions = [
      "ssmmessages:CreateControlChannel",
      "ssmmessages:CreateDataChannel",
      "ssmmessages:OpenControlChannel",
      "ssmmessages:OpenDataChannel",
    ]
  }

  statement {
    effect    = "Allow"
    resources = ["*"]

    actions = [
      "ec2messages:AcknowledgeMessage",
      "ec2messages:DeleteMessage",
      "ec2messages:FailMessage",
      "ec2messages:GetEndpoint",
      "ec2messages:GetMessages",
      "ec2messages:SendReply",
    ]
  }

  statement {
    effect    = "Allow"
    resources = ["*"]
    actions   = ["cloudwatch:PutMetricData"]
  }

  statement {
    effect    = "Allow"
    resources = ["*"]
    actions   = ["ec2:DescribeInstanceStatus"]
  }

  statement {
    effect    = "Allow"
    resources = ["*"]

    actions = [
      "ds:CreateComputer",
      "ds:DescribeDirectories",
    ]
  }

  statement {
    effect    = "Allow"
    resources = ["*"]

    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:DescribeLogGroups",
      "logs:DescribeLogStreams",
      "logs:PutLogEvents",
    ]
  }

  statement {
    effect    = "Allow"
    resources = ["*"]

    actions = [
      "s3:GetBucketLocation",
      "s3:PutObject",
      "s3:GetObject",
      "s3:GetEncryptionConfiguration",
      "s3:AbortMultipartUpload",
      "s3:ListMultipartUploadParts",
      "s3:ListBucket",
      "s3:ListBucketMultipartUploads",
    ]
  }
}

resource "aws_iam_policy" "ec2_role_for_ssm" {
  name_prefix = "ec2_role_for_ssm"
  policy      = data.aws_iam_policy_document.ec2_role_for_ssm.json
}

resource "aws_iam_role_policy_attachment" "ssm" {
  role       = aws_iam_role.default.id
  policy_arn = aws_iam_policy.ec2_role_for_ssm.arn
}

resource "aws_iam_role_policy" "ssm_parameters" {
  role   = aws_iam_role.default.*.id[0]
  policy = data.aws_iam_policy_document.ssm_putparameter.*.json[0]
}

data "aws_iam_policy_document" "ssm_putparameter" {
  statement {
    effect = "Allow"
    actions = [
      "kms:Encrypt",
      "kms:Decrypt",
      "kms:GenerateDataKey"
    ]
    resources = ["*"]
  }

  statement {
    effect = "Allow"
    actions = [
      "ssm:PutParameter",
      "ssm:DeleteParameter",
      "ssm:GetParameterHistory",
      "ssm:GetParametersByPath",
      "ssm:GetParameters",
      "ssm:GetParameter",
      "ssm:DeleteParameters"
    ]
    resources = ["arn:aws:ssm:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:parameter/*"]
  }

  statement {
    effect    = "Allow"
    actions   = ["ssm:DescribeParameters"]
    resources = ["*"]
  }
}

# RDS
resource "aws_iam_role_policy" "rds" {
  role   = aws_iam_role.default.*.id[0]
  policy = data.aws_iam_policy_document.rds.json
}

data "aws_iam_policy_document" "rds" {
  statement {
    effect = "Allow"
    actions = [
      "rds:DescribeDBInstances"
    ]
    resources = ["*"]
  }
}