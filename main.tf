resource "tls_private_key" "key_pair" {
  algorithm = "RSA"
}

resource "aws_key_pair" "key_pair" {
  key_name   = "jumpbox-${var.environment}"
  public_key = tls_private_key.key_pair.public_key_openssh
}


resource "aws_instance" "instance" {
  ami           = data.aws_ami.default.id
  instance_type = var.instance_type

  iam_instance_profile = aws_iam_instance_profile.default.name
  key_name             = aws_key_pair.key_pair.key_name

  vpc_security_group_ids = concat(
    var.security_group_ids,
    [aws_security_group.jumpbox.id]
  )
  subnet_id = var.subnet_id

  user_data = templatefile(
    "${path.module}/user_data.tmpl",
    {
      region = data.aws_region.current.name
    }
  )

  tags = merge(
    { "Name" : "jumpbox-${var.environment}" },
    local.tags
  )
}