# jumpbox

## Description
Launches an EC2 instance to be used as a connection to private VPC network via SSM

## Providers

| Name | Version |
|------|---------|
| aws | n/a |
| tls | n/a |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| environment | Name of the environment (Ex. `dev`, `uat`, `prod`) | `string` | `""` | no |
| instance\_type | What instance type to launch for Jumpbox | `string` | `"t2.micro"` | no |
| os\_platform | OS to launch with Jumpbox | `string` | `"linux"` | no |
| security\_group\_ids | List of Security Group IDs for Jumpbox | `list(string)` | `[]` | no |
| subnet\_id | Subnet ID to deploy the Jumpbox to | `any` | n/a | yes |
| tags | Tags to apply to the resources created by this module | `map(string)` | `{}` | no |
| vpc\_id | VPC ID for Jumpbox | `any` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| instance\_id | n/a |
| instance\_profile\_arns | n/a |
| security\_group\_id | n/a |

